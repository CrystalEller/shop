jQuery(document).ready(function () {
    var amount = $('#product-cart .products-amount');

    //Cookies.remove('cart', { path: '/' });

    $('button.buy').click(function () {
        var wrapper = $(this).closest('.product-wrapper');

        var products = Cookies.getJSON('cart');

        if (!products) {
            products = {};
            products[wrapper.data('product-id')] = 1;
        } else {
            if (products[wrapper.data('product-id')]) {
                products[wrapper.data('product-id')]++;
            } else {
                products[wrapper.data('product-id')] = 1;
            }
        }

        Cookies.set('cart', products, {path: '/'});

        amount.text(+amount.text() + 1);
    });

    $('.cart-remove-product').click(function () {
        var prods = Cookies.getJSON('cart');

        amount.text(+amount.text() - prods[$(this).data('product-id')]);

        delete prods[$(this).data('product-id')];
        $('#total-price .price').text(
            +$('#total-price .price').text() - $(this).closest('tr').find('.product-total-price').text());
        $(this).closest('tr').remove();


        Cookies.set('cart', prods, {path: '/'});
    });

    var prod = Cookies.getJSON('cart') ? Cookies.getJSON('cart') : [];

    for (var i in prod) {
        amount.text(+amount.text() + +prod[i]);
    }
});