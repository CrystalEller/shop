jQuery(document).ready(function () {
    $("#upload-image").change(function () {
        var input = this;

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image-wrapper img').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    });

    $("#image").click(function () {
        $("#upload-image").click();
    });
});