<?php

namespace Shop\Controller\Factory;


use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ShopControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $sm = $serviceLocator->getServiceLocator();
        $dep = $sm->get('doctrine.entitymanager.orm_default');
        $controller = new \Shop\Controller\ShopController($dep);

        return $controller;
    }
}