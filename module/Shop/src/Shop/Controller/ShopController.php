<?php

namespace Shop\Controller;

use Application\Entity\Order;
use Application\Entity\ProductOrder;
use Doctrine\ORM\EntityManager;
use Zend\Http\Header\SetCookie;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ShopController extends AbstractActionController
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function indexAction()
    {
        $products = $this->em->getRepository('Application\Entity\Product')->findAll();

        return new ViewModel(['products' => $products]);
    }

    public function categoryAction()
    {
        $cid = $this->params()->fromRoute('id');

        $products = $this->em
            ->createQueryBuilder()
            ->select('p', 'c')
            ->from('Application\Entity\Product', 'p')
            ->innerJoin('p.category', 'c')
            ->where('c.id = :id')
            ->setParameter('id', $cid)
            ->getQuery()
            ->getResult();

        return new ViewModel(['products' => $products]);
    }

    public function productAction()
    {
        $cid = $this->params()->fromRoute('id');

        $product = $this->em->find('Application\Entity\Product', $cid);

        return new ViewModel(['product' => $product]);
    }

    public function shoppingCartAction()
    {
        $request = $this->getRequest();

        $this->layout('layout/shopping-cart');
        $cookie = (array)$request->getCookie();


        if ($request->isPost()) {
            $filter = $this->getServiceLocator()->get('Shop\Filter\Order');
            $filter->setData($request->getPost());

            if (!$filter->isValid()) {
                return new ViewModel(['errors' => $filter->getMessages()]);
            } else {

                if (array_key_exists('cart', $cookie)) {
                    $prods = json_decode($cookie['cart'], true);
                    if (sizeof($prods) <= 0) {
                        return new ViewModel(['emptyCart' => 'cart is empty']);
                    } else {
                        $order = new Order();
                        $order->setName($filter->getRawValue('name'));
                        $order->setTelephone($filter->getRawValue('telephone'));
                        $order->setEmail($filter->getRawValue('email'));
                        $order->setAddress($filter->getRawValue('address'));

                        $this->em->persist($order);

                        $products = $this->em
                            ->createQueryBuilder()
                            ->select('p')
                            ->from('Application\Entity\Product', 'p')
                            ->where('p.id IN (:ids)')
                            ->setParameter('ids', array_keys($prods))
                            ->getQuery()
                            ->getResult();

                        if (sizeof($products) == sizeof($prods)) {
                            $this->em->flush();

                            foreach ($products as $product) {
                                $productOrder = new ProductOrder();
                                $productOrder->setAmout(intval($prods[$product->getId()]));
                                $productOrder->setOrder($order);
                                $productOrder->setProduct($product);

                                $this->em->persist($productOrder);
                            }
                        }

                        $this->em->flush();


                        $cookie = new SetCookie(
                            'cart',
                            '',
                            strtotime('-1 Year', time()),
                            '/'
                        );

                        $this->getServiceLocator()->get('Response')->getHeaders()->addHeader($cookie);
                    }
                }

                return $this->redirect()->toUrl('/');
            }
        }

        if (array_key_exists('cart', $cookie)) {
            $prods = json_decode($cookie['cart'], true);

            $products = $this->em
                ->createQueryBuilder()
                ->select('p')
                ->from('Application\Entity\Product', 'p')
                ->where('p.id IN (:ids)')
                ->setParameter('ids', array_keys($prods))
                ->getQuery()
                ->getResult();


            return new ViewModel(['products' => $products, 'cart' => $prods]);
        }

        return new ViewModel([]);
    }
}
