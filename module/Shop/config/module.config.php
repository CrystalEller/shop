<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'Shop\Controller\Shop',
                        'action' => 'index',
                    ),
                ),
            ),
            'category' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/category/:id',
                    'constraints' => array(
                        'id' => '\d+',
                    ),
                    'defaults' => array(
                        'controller' => 'Shop\Controller\Shop',
                        'action' => 'category',
                    ),
                ),
            ),
            'product' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/product/:id',
                    'constraints' => array(
                        'id' => '\d+',
                    ),
                    'defaults' => array(
                        'controller' => 'Shop\Controller\Shop',
                        'action' => 'product',
                    ),
                ),
            ),
            'shopping-cart' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/shopping-cart',
                    'defaults' => array(
                        'controller' => 'Shop\Controller\Shop',
                        'action' => 'shopping-cart',
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'invokables' => array(
            'Shop\Filter\Order' => 'Shop\Filter\OrderFilter',
        ),
    ),
    'controllers' => array(
        'factories' => array(
            'Shop\Controller\Shop' => 'Shop\Controller\Factory\ShopControllerFactory',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(),
        ),
    ),
);
