<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProductOrder
 *
 * @ORM\Table(name="product_order", indexes={@ORM\Index(name="product_fk_idx", columns={"product_id"}), @ORM\Index(name="order_fk_idx", columns={"order_id"})})
 * @ORM\Entity
 */
class ProductOrder
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="amout", type="integer", nullable=true)
     */
    private $amout;

    /**
     * @var \Application\Entity\Product
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     */
    private $product;

    /**
     * @var \Application\Entity\Order
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Order", inversedBy="productOrder")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     * })
     */
    private $order;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amout
     *
     * @param integer $amout
     *
     * @return ProductOrder
     */
    public function setAmout($amout)
    {
        $this->amout = $amout;

        return $this;
    }

    /**
     * Get amout
     *
     * @return integer
     */
    public function getAmout()
    {
        return $this->amout;
    }

    /**
     * Set product
     *
     * @param \Application\Entity\Product $product
     *
     * @return ProductOrder
     */
    public function setProduct(\Application\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Application\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set order
     *
     * @param \Application\Entity\Order $order
     *
     * @return ProductOrder
     */
    public function setOrder(\Application\Entity\Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return \Application\Entity\Order
     */
    public function getOrder()
    {
        return $this->order;
    }
}
