<?php

return array(
    'router' => array(
        'routes' => array(
            'admin' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/admin',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller' => 'Admin',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'admin-category' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/category[/:action][/:id]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '\d+',
                            ),
                            'defaults' => array(
                                'controller' => 'Category',
                            ),
                        ),
                    ),
                    'admin-product' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/product[[/:action][/:id]]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '\d+',
                            ),
                            'defaults' => array(
                                'controller' => 'Product',
                            ),
                        ),
                    ),
                    'admin-order' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/order[[/:action][/:id]]',
                            'constraints' => array(
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '\d+',
                            ),
                            'defaults' => array(
                                'controller' => 'Order',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        /*        'invokables' => array(
                    'Admin\Filter\Product' => 'Admin\Filter\ProductFilter',
                    'Admin\Filter\Order' => 'Admin\Filter\OrderFilter',
                ),*/
        'factories' => array(
            'Admin\Filter\Category' => 'Admin\Filter\Factory\CategoryFilterFactory',
            'Admin\Filter\Product' => 'Admin\Filter\Factory\ProductFilterFactory',
            'Admin\Filter\Order' => 'Admin\Filter\Factory\OrderFilterFactory',
        ),
    ),
    'controllers' => array(
        'factories' => array(
            'Admin\Controller\Admin' => 'Admin\Controller\Factory\AdminControllerFactory',
            'Admin\Controller\Category' => 'Admin\Controller\Factory\CategoryControllerFactory',
            'Admin\Controller\Product' => 'Admin\Controller\Factory\ProductControllerFactory',
            'Admin\Controller\Order' => 'Admin\Controller\Factory\OrderControllerFactory',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),

    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(),
        ),
    ),
);
