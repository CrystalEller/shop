<?php

namespace Admin\Controller\Factory;


use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AdminControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $sm = $serviceLocator->getServiceLocator();
        $dep = $sm->get('doctrine.entitymanager.orm_default');
        $controller = new \Admin\Controller\AdminController($dep);

        return $controller;
    }
}