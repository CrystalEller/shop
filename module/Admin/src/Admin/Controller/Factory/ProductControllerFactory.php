<?php

namespace Admin\Controller\Factory;


use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ProductControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $sm = $serviceLocator->getServiceLocator();
        $dep = $sm->get('doctrine.entitymanager.orm_default');
        $controller = new \Admin\Controller\ProductController($dep);

        return $controller;
    }
}