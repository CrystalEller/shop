<?php

namespace Admin\Controller;


use Application\Entity\Category;
use Doctrine\ORM\EntityManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class CategoryController extends AbstractActionController
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function indexAction()
    {
        $categories = $this->em->getRepository('Application\Entity\Category')->findAll();

        return new ViewModel(['categories' => $categories]);
    }

    public function createAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $filter = $this->getServiceLocator()->get('Admin\Filter\Category');
            $filter->setData($request->getPost());

            if (!$filter->isValid()) {
                return new ViewModel(['errors' => $filter->getMessages()]);
            } else {
                $category = new Category();
                $category->setName($filter->getRawValue('name'));

                $this->em->persist($category);
                $this->em->flush();

                return $this->redirect()->toUrl('/admin/category');
            }
        }
        return new ViewModel();
    }

    public function updateAction()
    {
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        $entity = $this->em->find('Application\Entity\Category', $id);

        if (empty($entity)) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        if ($request->isPost()) {
            $filter = $this->getServiceLocator()->get('Admin\Filter\Category');
            $filter->setData($request->getPost());

            if (!$filter->isValid()) {
                return new ViewModel(['errors' => $filter->getMessages(), 'category' => $entity]);
            } else {
                $entity->setName($filter->getRawValue('name'));
                $this->em->merge($entity);
                $this->em->flush();

                return $this->redirect()->toUrl('/admin/category');
            }
        }

        return new ViewModel(['category' => $entity]);
    }

    public function deleteAction()
    {
        $id = $this->params()->fromRoute('id');
        $entity = $this->em->find('Application\Entity\Category', $id);

        if (empty($entity)) {
            $this->getResponse()->setStatusCode(404);
            return;
        } else {
            $this->em->remove($entity);
            $this->em->flush();

            return $this->redirect()->toUrl('/admin/category');
        }
    }
}