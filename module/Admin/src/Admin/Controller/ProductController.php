<?php

namespace Admin\Controller;


use Application\Entity\Product;
use Doctrine\ORM\EntityManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Validator\File\Size;
use Zend\View\Model\ViewModel;

class ProductController extends AbstractActionController
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function indexAction()
    {
        $products = $this->em->getRepository('Application\Entity\Product')->findAll();

        return new ViewModel(['products' => $products]);
    }

    public function createAction()
    {
        $request = $this->getRequest();

        $categories = $this->em->getRepository('Application\Entity\Category')->findAll();

        if ($request->isPost()) {
            $filter = $this->getServiceLocator()->get('Admin\Filter\Product');
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );
            $filter->setData($post);

            if (!$filter->isValid()) {
                return new ViewModel(['errors' => $filter->getMessages(), 'categories' => $categories]);
            } else {
                $product = new Product();
                $product->setTitle($filter->getRawValue('title'));
                $product->setDescription($filter->getRawValue('description'));
                $product->setCategory($this->em->find('Application\Entity\Category', $filter->getRawValue('category')));
                $product->setPrice($filter->getRawValue('price'));

                $this->em->persist($product);
                $this->em->flush();

                if (!empty($filter->getRawValue('img'))) {
                    $img = $filter->getRawValue('img');
                    $dir = "./public/img/product/" . $product->getId() . "/";
                    mkdir($dir);
                    $re = new \Zend\Filter\File\Rename(array(
                        "target" => $dir . "img." . pathinfo($img['name'], PATHINFO_EXTENSION),
                        "randomize" => true,
                    ));
                    $re->filter($img);
                }

                return $this->redirect()->toUrl('/admin/product');
            }
        }

        return new ViewModel(['categories' => $categories]);
    }

    public function updateAction()
    {
        $request = $this->getRequest();
        $id = $this->params()->fromRoute('id');
        $entity = $this->em->find('Application\Entity\Product', $id);

        if (empty($entity)) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        $categories = $this->em->getRepository('Application\Entity\Category')->findAll();

        if ($request->isPost()) {
            $filter = $this->getServiceLocator()->get('Admin\Filter\Product');
            $post = array_merge_recursive(
                $request->getPost()->toArray(),
                $request->getFiles()->toArray()
            );

            $filter->setData($post);

            if (!$filter->isValid()) {
                return new ViewModel(['errors' => $filter->getMessages(), 'product' => $entity, 'categories' => $categories]);
            } else {
                $entity->setTitle($filter->getRawValue('title'));
                $entity->setDescription($filter->getRawValue('description'));
                $entity->setCategory($this->em->find('Application\Entity\Category', $filter->getRawValue('category')));
                $entity->setPrice($filter->getRawValue('price'));

                $this->em->merge($entity);
                $this->em->flush();

                if (!empty($filter->getRawValue('img'))) {
                    $img = $filter->getRawValue('img');
                    $dir = "./public/img/product/" . $entity->getId() . "/";
                    $files = glob($dir . '*');
                    foreach ($files as $file) {
                        if (is_file($file))
                            unlink($file);
                    }
                    $re = new \Zend\Filter\File\Rename(array(
                        "target" => $dir . "img." . pathinfo($img['name'], PATHINFO_EXTENSION),
                        "randomize" => true,
                    ));
                    $re->filter($img);
                }

                return $this->redirect()->toUrl('/admin/product');
            }
        }

        return new ViewModel(['categories' => $categories, 'product' => $entity]);
    }

    public function deleteAction()
    {
        $id = $this->params()->fromRoute('id');
        $entity = $this->em->find('Application\Entity\Product', $id);

        if (empty($entity)) {
            $this->getResponse()->setStatusCode(404);
            return;
        } else {
            $this->em->remove($entity);
            $this->em->flush();

            return $this->redirect()->toUrl('/admin/product');
        }
    }
}