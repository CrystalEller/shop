<?php

namespace Admin\Controller;

use Application\Entity\Category;
use Doctrine\ORM\EntityManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class OrderController extends AbstractActionController
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function indexAction()
    {
        $orders = $this->em->getRepository('Application\Entity\Order')->findAll();

        return new ViewModel(['orders' => $orders]);
    }

    public function deleteAction()
    {
        $id = $this->params()->fromRoute('id');
        $entity = $this->em->find('Application\Entity\Order', $id);

        if (empty($entity)) {
            $this->getResponse()->setStatusCode(404);
            return;
        } else {
            $this->em->remove($entity);
            $this->em->flush();

            return $this->redirect()->toUrl('/admin/order');
        }
    }
}