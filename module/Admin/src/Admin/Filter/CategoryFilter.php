<?php

namespace Admin\Filter;


use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;

class CategoryFilter extends InputFilter implements InputFilterInterface
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->add(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim',
                )
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 40,
                    ),
                ),
                array(
                    'name' => '\DoctrineModule\Validator\NoObjectExists',
                    'options' => array(
                        'object_repository' => $em->getRepository('\Application\Entity\Category'),
                        'fields' => 'name',
                        'messages' => array(
                            'objectFound' => 'This category name already exist'
                        ),
                    ),
                )
            )
        ));
    }
}