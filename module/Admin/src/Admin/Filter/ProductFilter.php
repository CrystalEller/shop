<?php

namespace Admin\Filter;


use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterInterface;

class ProductFilter extends InputFilter implements InputFilterInterface
{
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->add(array(
            'name' => 'title',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim',
                )
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 50,
                    ),
                ),
            )
        ));
        $this->add(array(
            'name' => 'description',
            'required' => true,
            'filters' => array(
                array(
                    'name' => 'StringTrim',
                )
            ),
            'validators' => array(
                array(
                    'name' => 'StringLength',
                    'options' => array(
                        'encoding' => 'UTF-8',
                        'min' => 3,
                        'max' => 5000,
                    ),
                ),
            )
        ));
        $this->add(array(
            'name' => 'category',
            'required' => true,
            'validators' => array(
                array(
                    'name' => '\DoctrineModule\Validator\ObjectExists',
                    'options' => array(
                        'object_repository' => $em->getRepository('\Application\Entity\Category'),
                        'fields' => 'id',
                        'messages' => array(
                            'noObjectFound' => 'This category dose not exist'
                        ),
                    ),
                )
            )
        ));
        $this->add(array(
            'name' => 'price',
            'required' => true,
            'validators' => array(
                array(
                    'name' => 'Between',
                    'options' => array(
                        'min' => 1,
                        'max' => 100000000,
                        'inclusive' => false
                    ),
                )
            )
        ));
        $this->add(array(
            'name' => 'img',
            'type' => 'Zend\InputFilter\FileInput',
            'required' => false,
            'validators' => array(
                array(
                    'name' => 'Zend\Validator\File\Extension',
                    'options' => array(
                        'extension' => 'jpeg,jpg,png'
                    )
                ),
                array(
                    'name' => 'Zend\Validator\File\Size',
                    'options' => array(
                        'max' => '5MB',
                        'min' => '10kB'
                    )
                ),
            )
        ));
    }
}