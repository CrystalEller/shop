<?php

namespace Admin\Filter\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class CategoryFilterFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        //$sm   = $serviceLocator->getServiceLocator();
        $dep = $serviceLocator->get('doctrine.entitymanager.orm_default');
        $filter = new \Admin\Filter\CategoryFilter($dep);

        return $filter;
    }
}